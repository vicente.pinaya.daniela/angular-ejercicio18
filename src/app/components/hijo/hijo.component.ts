import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

    //Comunicacion Input padre-hijo -OBJETOS
    @Input() objetoPadre_hijo1!:any;
    @Input() objetoPadre_hijo2!:any;

  constructor() { }

  ngOnInit(): void {
  }

}
