import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

     //Comunicacion Input padre-hijo -OBJETOS
     animales = {
      mamiferos:'leon, hipopotamo, osoPolar',
      oviparos:'pato, gallina, tortuga',
      viviparos:'caballo,oso'
    }
  
    colores = {
      primarios:'amarillo, azul y rojo.',
      secundarios:'el verde, naranja y violeta',
      terciarios:'color primario + un color secundario'
    }

  constructor() { }

  ngOnInit(): void {
  }

}
